<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> AbmInmueble </title>
    <link rel="stylesheet" href="assets/css/estilos.css">
    
</head>
<body>
    
    <main>
         <div class="Titulo"> 
            <h2> CAPUCCHIO PROPIEDADES</h2>   
            
        </div>

    <div class="Inmuebles">
    
         <form action="php/abminmueble.php" method="POST" class="formulario__inmuebles">
         
                        <h2>Ingrese su inmueble</h2>

                        <h3>Nombres del titular </h3>
                        <input type="text" placeholder="Nombres" name="nombre">

                        <h3>Apellidos del titular</h3>
                        <input type="text" placeholder="Apellidos" name="apellido">  

                        <h3>Direccion</h3>
                        <input type="text" placeholder="Direccion" name="direccion">  

                        <h3>Localidad</h3> 
                        <input type="text" placeholder="Localidad" name="localidad"> 

                        <h3>Numero de telefono</h3>
                        <input type="text" placeholder="Telefono" name="telefono">

                        <h3>Cantidad de m2 del inmueble</h3>
                        <input type="number" step="any"  placeholder="Cantidad exacta de M2 " name="m2"> <!-- con el any podemos poner N° decimales-->

                        <button>ENVIAR</button>

            </form>
    </div>
    </main>


</body>
</html>